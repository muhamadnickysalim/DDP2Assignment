package game.action;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import game.Main;
import game.gui.*;

public class Process implements ActionListener { //what the program to do when two card opened compared here
	PictureButton firstButton;
	PictureButton lastButton;
	
	public Process(PictureButton first, PictureButton last) { //constructor
		this.firstButton = first;
		this.lastButton = last;
	}
	
	public void actionPerformed(ActionEvent e) {
		if (firstButton.getId() != lastButton.getId()) { // if the pic is same (known by id)
			lastButton.setIcon(lastButton.getStandar());
			lastButton.chgButtonStatus();
			firstButton.setIcon(firstButton.getStandar());
			firstButton.chgButtonStatus();
		} else { // if not same
			JPanel temp = Main.layout.getButtonPanel();
			temp.remove(lastButton);
			temp.remove(firstButton);
		}
	}
}