package game.action;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import game.gui.*;

public class PictureClick implements ActionListener {
	private PictureButton selected;
	
	public PictureClick(PictureButton selected) {
		this.selected = selected; 
	}

	public void actionPerformed(ActionEvent e) {
		if (!this.selected.getButtonStatus()) {
			this.selected.showPic();
			this.selected.chgButtonStatus();
		}
	}
}
