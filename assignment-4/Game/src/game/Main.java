package game;

import game.gui.*; 

public class Main {
	public static Layout layout;
	
	public static void main(String[] args) {
		layout = new Layout();
	}
	
	public static void restart() {
		layout = null;
		System.gc();
		layout = new Layout();
	}

}

//Picture credited for : https://berkahkhair.com/hewan-terkecil-di-dunia/