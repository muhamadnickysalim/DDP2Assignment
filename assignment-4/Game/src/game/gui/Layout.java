package game.gui;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;

import game.*;
import game.action.*;
import game.action.Process;

public class Layout {
	private JFrame window;
	private ArrayList<JButton> buttonList = new ArrayList<JButton>();
	private int imageSize = 80;
	private Dimension size = new Dimension(6*imageSize,7*imageSize);
	private JButton restart;
	private int tried = 0;
	private JLabel status;
	private String status_string = "You have tried 0 time(s)";
	private boolean state = false;
	private PictureButton activeButton;
	private JPanel buttonPanel;
	
	public Layout() {
		window = new JFrame();
		window.setTitle("Match Pair Game");
		window.setSize(size);
		window.setMinimumSize(size);
		window.setLayout(new BorderLayout());
		setPictureGrid();
		setBottom();
		
		window.pack();
		window.setVisible(true);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	private void setPictureGrid() {
		buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(6,6));
		for (int i = 0; i < 36; i++) {
			PictureButton temp = new PictureButton(i/2);
			temp.addActionListener((new PictureClick(temp)));
			buttonList.add(temp);
		}
		Collections.shuffle(buttonList);
		for (int i = 0; i < 36; i++) {
			buttonPanel.add((Component) buttonList.get(i));
		}
		window.add(buttonPanel,BorderLayout.CENTER);	
	}
	
	private void setBottom() {
		JPanel bottom = new JPanel();
		bottom.setLayout(new BorderLayout());
		
		restart = new JButton("Restart");
		restart.addActionListener(e -> restart());
		bottom.add(restart, BorderLayout.CENTER);
		
		status = new JLabel(status_string);
		bottom.add(status, BorderLayout.SOUTH);
		window.add(bottom, BorderLayout.SOUTH);
	}
	
	public void process(PictureButton lastClicked) {
		Timer action = new Timer(800, new Process(lastClicked, activeButton));
		action.setRepeats(false);
		action.restart();
		activeButton = null;
	}
	
	private void restart() {
		window.dispose();
		Main.restart();
	}
	
	public JPanel getButtonPanel() {
		return this.buttonPanel;
	}
	
	public PictureButton getActiveButton() {
		return this.activeButton;
	}
	
	public void chgActiveButton(PictureButton temp) {
		this.activeButton = temp;
	}
	
	public boolean getState() {
		return state;
	}
	
	public void chgState(boolean state) {
		this.state = state;
	}
	
	public int getTried(){
		return tried;
	}
	
	public void chgTried(int i) {
		this.tried = i;
	}
	
	public void updateStatus() {
		status.setText("You have tried " + tried + " time(s)");
	}
	
}