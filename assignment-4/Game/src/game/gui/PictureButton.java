package game.gui;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import game.*;

public class PictureButton extends JButton { // this is the button tipe for picture button
	private ImageIcon inside; // what the image inside the button (was hidden)
	private ImageIcon standar = new ImageIcon("ico/default.png"); //set default icon for disguise
	private int id; // for comparing one button to other button. there always 2 buttons which have the same id
	private boolean buttonStatus; // is opened / not

	public boolean getButtonStatus() {
		return this.buttonStatus;
	}
	
	public int getId() {
		return this.id;
	}
	
	public ImageIcon getStandar() {
		return standar;
	}
	
	public void chgButtonStatus() {
		this.buttonStatus = !this.buttonStatus;
	}
	
	public PictureButton(int i) { //construct the button type with extended variable needed
		super();
		this.id = i;
		this.inside = new ImageIcon("ico/" + i + ".png"); //the hidden picture was given
		setIcon(standar); //show the default icon for disguising
		buttonStatus = false; // init the default status of opened (the button is opened is false)
	}
	
	public void showPic(){
		setIcon(inside); //show the hidden pic 
		if (Main.layout.getState() == false) { // if no one picture opened before
			Main.layout.chgState(true);
			Main.layout.chgActiveButton(this);
		} else { // if ne picture is opened
			Main.layout.chgTried(Main.layout.getTried()+1);
			Main.layout.chgState(false);
			Main.layout.updateStatus();
			Main.layout.process(this);
		}
	}

}
