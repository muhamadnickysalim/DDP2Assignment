import java.util.Scanner;
public class JavariPark{
	public static boolean play = true;
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		System.out.println("Welcome to Javari Park!");
		System.out.println("Input the number of animals\nCat: ");
		int total = Integer.parseInt(input.nextLine());
		if (total > 0){
			System.out.println("Provide the information of cat(s):");
			String temp = input.nextLine();
			DataManipulation.splittingString(temp, "cat");
		}
		System.out.println("Lion: ");
		total = Integer.parseInt(input.nextLine());
		if (total > 0){
			System.out.println("Provide the information of lion(s):");
			String temp = input.nextLine();
			DataManipulation.splittingString(temp, "lion");
		}
		System.out.println("Eagle: ");
		total = Integer.parseInt(input.nextLine());
		if (total > 0){
			System.out.println("Provide the information of eagle(s):");
			String temp = input.nextLine();
			DataManipulation.splittingString(temp, "eagle");
		}
		System.out.println("Parrot: ");
		total = Integer.parseInt(input.nextLine());
		if (total > 0){
			System.out.println("Provide the information of parrot(s):");
			String temp = input.nextLine();
			DataManipulation.splittingString(temp, "parrot");
		}
		System.out.println("Hamster: ");
		total = Integer.parseInt(input.nextLine());
		if (total > 0){
			System.out.println("Provide the information of hamster(s):");
			String temp = input.nextLine();
			DataManipulation.splittingString(temp, "hamster");
		}
		
		System.out.println("Animals have been successfully recorded!");
		System.out.println("=============================================");
		System.out.println("Cage arrangement:");
		DataManipulation.catPosition(DataManipulation.cats, "indoor");
		DataManipulation.lionPosition(DataManipulation.lions, "outdoor");
		DataManipulation.eaglePosition(DataManipulation.eagles, "outdoor");
		DataManipulation.parrotPosition(DataManipulation.parrots, "indoor");
		DataManipulation.hamsterPosition(DataManipulation.hamsters, "indoor");
		
		System.out.println("=============================================");
		while(play){
			Interact.play();
		}
	}
}