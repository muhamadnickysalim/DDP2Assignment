import java.util.Scanner;
public class Interact{
	public static void play(){
		Scanner input = new Scanner(System.in);
		System.out.println("Which animal you want to visit?");
		System.out.println("(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99:exit)");
		int choice = Integer.parseInt(input.nextLine());
		if (!(choice == 1 || choice == 2 || choice == 3 || choice == 4 || choice == 5 || choice == 99)){
			System.out.println("You choose nothing...\nBack to the office!");
		}
		else if (choice == 99){
			System.out.println("Have a nice day...");
			JavariPark.play = false;
			input.nextLine();
		}
		else{
			if (choice == 1){
				String animal = "cat";
			}
			else if (choice == 2){
				String animal = "eagle";
			}
			else if (choice == 3){
				String animal = "hamster";
			}
			else if (choice == 4){
				String animal = "parrot";
			}
			else{
				String animal = "lion";
			}
			
			System.out.print("Mention the name of animal you want to visit: ");
			String nama = input.nextLine();
			boolean result;
			if (choice == 1){
				result = DataManipulation.search(nama,DataManipulation.cats_name);
			}
			else if (choice == 2){
				result = DataManipulation.search(nama,DataManipulation.eagles_name);
			}
			else if (choice == 3){
				result = DataManipulation.search(nama,DataManipulation.hamsters_name);
			}
			else if (choice == 4){
				result = DataManipulation.search(nama,DataManipulation.parrots_name);
			}
			else{
				result = DataManipulation.search(nama,DataManipulation.lions_name);
			}
			
			if (!result){
				System.out.print("There is no animal with that name! ");
			}
			else{
				if (choice == 1){
					Cat.visited(nama);
					Cat.answer(nama, Integer.parseInt(input.nextLine()));
				}
				else if (choice == 2){
					Eagle.visited(nama);
					Eagle.answer(nama, Integer.parseInt(input.nextLine()));
				}
				else if (choice == 3){
					Hamster.visited(nama);
					Hamster.answer(nama, Integer.parseInt(input.nextLine()));
				}
				else if (choice == 4){
					Parrot.visited(nama);
					Parrot.answer(nama, Integer.parseInt(input.nextLine()));
				}
				else{
					Lion.visited(nama);
					Lion.answer(nama, Integer.parseInt(input.nextLine()));
				}
			}
			System.out.println("Back to the office!\n");
		}
	}
}