public class Lion{
	private String name;
	private int length;
	private char cage;
	
	public Lion(String name, int length, char cage){
		this.name = name;
		this.length = length;
		this.cage = cage;
	}
	public String getName(){
		return this.name;
	}
	public int getLength(){
		return this.length;
	}
	public char getCage(){
		return this.cage;
	}
	public String toString(){
		return this.getName() + " (" + this.getLength() + " - " + this.getCage() + ")";
	}
	public static String visited(String name){
		return "You are visiting " + name + " (lion) now, what would you like to do? \n1: See it hunting 2: Brush the mane 3: Disturb it\n";
	}
	public static void answer(String name, int n){
		if (n == 1){
			System.out.println("Lion is hunting..");
			System.out.println(name + " makes a voice: err...!");
		}
		else if (n == 2){
			System.out.println("Clean the lion's mane..");
			System.out.println(name + " makes a voice: Hauhhmm!");
		}
		else if (n == 3){
			System.out.println(name + " makes a voice: HAUHHMM!");
		}
		else{
			System.out.println("You do nothing...");
		}
	}	
}