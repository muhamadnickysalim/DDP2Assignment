import java.util.concurrent.ThreadLocalRandom;
public class Cat{
	private String name;
	private int length;
	private char cage;
	private static String[] voices = new String[]{ "Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};
	
	public Cat(String name, int length, char cage){
		this.name = name;
		this.length = length;
		this.cage = cage;
	}
	public String getName(){
		return this.name;
	}
	public int getLength(){
		return this.length;
	}
	public char getCage(){
		return this.cage;
	}
	public String toString(){
		return this.getName() + " (" + this.getLength() + " - " + this.getCage() + ")";
	}
	public static String visited(String name){
		return "You are visiting " + name + " (cat) now, what would you like to do? \n1: Brush the fur 2: Cuddle\n";
	}
	public static void answer(String name, int n){
		if (n == 1){
			System.out.println("Time to clean " + name + "'s fur");
			System.out.println(name + " makes a voice: Nyaaan...");
		}
		else if (n == 2){
			System.out.print(name + " makes a voice: ");
			System.out.println(voices[ThreadLocalRandom.current().nextInt(0, 4)]);
		}
		else{
			System.out.println("You do nothing...");
		}
	}	
}