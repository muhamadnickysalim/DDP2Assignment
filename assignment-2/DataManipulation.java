import java.util.ArrayList;
public class DataManipulation{
	public static ArrayList<Cat> cats = new ArrayList<Cat>();
	public static ArrayList<Hamster> hamsters = new ArrayList<Hamster>();
	public static ArrayList<Parrot> parrots = new ArrayList<Parrot>();
	public static ArrayList<Eagle> eagles = new ArrayList<Eagle>();
	public static ArrayList<Lion> lions = new ArrayList<Lion>();
	
	public static ArrayList<String> cats_name = new ArrayList<String>();
	public static ArrayList<String> hamsters_name = new ArrayList<String>();
	public static ArrayList<String> parrots_name = new ArrayList<String>();
	public static ArrayList<String> eagles_name = new ArrayList<String>();
	public static ArrayList<String> lions_name = new ArrayList<String>();
	
	public static boolean search(String nama, ArrayList<String> data){
		for (int i = 0; i<data.size(); i++){
			String temp = data.get(i);
			if (temp.equals(nama))
				return true;
			}
		return false;	
	}
	
	public static void splittingString(String input, String animal){
		String[] temp =  input.split(",");
		if (animal == "cat"){
			for (int i = 0; i<temp.length; i++){
				String[] animalID = temp[i].split("|");
				int n = Integer.parseInt(animalID[1]);
				cats.add(new Cat(animalID[0], n, Cage.indoor(n)));
				cats_name.add(animalID[0]);
			}
		}
		else if (animal == "hamster"){
			for (int i = 0; i<temp.length; i++){
				String[] animalID = temp[i].split("|");
				int n = Integer.parseInt(animalID[1]);
				hamsters.add(new Hamster(animalID[0], n, Cage.indoor(n)));
				hamsters_name.add(animalID[0]);
			}
		}
		else if (animal == "parrot"){
			for (int i = 0; i<temp.length; i++){
				String[] animalID = temp[i].split("|");
				int n = Integer.parseInt(animalID[1]);
				parrots.add(new Parrot(animalID[0], n, Cage.indoor(n)));
				parrots_name.add(animalID[0]);
			}
		}
		else if (animal == "eagle"){
			for (int i = 0; i<temp.length; i++){
				String[] animalID = temp[i].split("|");
				int n = Integer.parseInt(animalID[1]);
				eagles.add(new Eagle(animalID[0], n, Cage.indoor(n)));
				eagles_name.add(animalID[0]);
			}
		}
		else{
			for (int i = 0; i<temp.length; i++){
				String[] animalID = temp[i].split("|");
				int n = Integer.parseInt(animalID[1]);
				lions.add(new Lion(animalID[0], n, Cage.indoor(n)));
				lions_name.add(animalID[0]);
			}
		}
	}	
	public static void catPosition(ArrayList<Cat> data, String place){
		int total = data.size();
		if (total == 0)
			return;
		int perCage = total / 3;
		ArrayList<String> layer1 = new ArrayList<String>();
		ArrayList<String> layer2 = new ArrayList<String>();
		ArrayList<String> layer3 = new ArrayList<String>();
		int cage2 = 2*perCage + ((total%3)/2);
		for (int i = 0; i < total; i++){
			if (i < perCage){
				layer1.add(data.get(i).toString());
			}
			else if (i < cage2){
				layer2.add(data.get(i).toString());
			}
			else{
				layer3.add(data.get(i).toString());
			}
		}
		
		System.out.println("Location: " + place);
		System.out.print("Level 3:");
		for (int i = 0; i<layer3.size(); i++){
			System.out.print(" " + layer3.get(i));
		}
		System.out.println();
		System.out.print("Level 2:");
		for (int i = 0; i<layer2.size(); i++){
			System.out.print(" " + layer2.get(i));
		}
		System.out.println();
		System.out.print("Level 1:");
		for (int i = 0; i<layer1.size(); i++){
			System.out.print(" " + layer1.get(i));
		}
		System.out.println("\n");
		
		System.out.println(" After rearrangement...");
		System.out.print("Level 3:");
		for (int i = layer2.size()-1; i>= 0; i--){
			System.out.print(" " + layer2.get(i));
		}
		System.out.println();
		System.out.print("Level 2:");
		for (int i = layer1.size()-1; i>= 0; i--){
			System.out.print(" " + layer1.get(i));
		}
		System.out.println();
		System.out.print("Level 1:");
		for (int i = 0; i<layer3.size(); i++){
			System.out.print(" " + layer3.get(i));
		}
		System.out.println("\n");
	}
	public static void hamsterPosition(ArrayList<Hamster> data, String place){
		int total = data.size();
		if (total == 0)
			return;
		int perCage = total / 3;
		ArrayList<String> layer1 = new ArrayList<String>();
		ArrayList<String> layer2 = new ArrayList<String>();
		ArrayList<String> layer3 = new ArrayList<String>();
		int cage2 = 2*perCage + ((total%3)/2);
		for (int i = 0; i < total; i++){
			if (i < perCage){
				layer1.add(data.get(i).toString());
			}
			else if (i < cage2){
				layer2.add(data.get(i).toString());
			}
			else{
				layer3.add(data.get(i).toString());
			}
		}
		
		System.out.println("Location: " + place);
		System.out.print("Level 3:");
		for (int i = 0; i<layer3.size(); i++){
			System.out.print(" " + layer3.get(i));
		}
		System.out.println();
		System.out.print("Level 2:");
		for (int i = 0; i<layer2.size(); i++){
			System.out.print(" " + layer2.get(i));
		}
		System.out.println();
		System.out.print("Level 1:");
		for (int i = 0; i<layer1.size(); i++){
			System.out.print(" " + layer1.get(i));
		}
		System.out.println("\n");
		
		System.out.println(" After rearrangement...");
		System.out.print("Level 3:");
		for (int i = layer2.size()-1; i>= 0; i--){
			System.out.print(" " + layer2.get(i));
		}
		System.out.println();
		System.out.print("Level 2:");
		for (int i = layer1.size()-1; i>= 0; i--){
			System.out.print(" " + layer1.get(i));
		}
		System.out.println();
		System.out.print("Level 1:");
		for (int i = 0; i<layer3.size(); i++){
			System.out.print(" " + layer3.get(i));
		}
		System.out.println("\n");
	}
	public static void parrotPosition(ArrayList<Parrot> data, String place){
		int total = data.size();
		if (total == 0)
			return;
		int perCage = total / 3;
		ArrayList<String> layer1 = new ArrayList<String>();
		ArrayList<String> layer2 = new ArrayList<String>();
		ArrayList<String> layer3 = new ArrayList<String>();
		int cage2 = 2*perCage + ((total%3)/2);
		for (int i = 0; i < total; i++){
			if (i < perCage){
				layer1.add(data.get(i).toString());
			}
			else if (i < cage2){
				layer2.add(data.get(i).toString());
			}
			else{
				layer3.add(data.get(i).toString());
			}
		}
		
		System.out.println("Location: " + place);
		System.out.print("Level 3:");
		for (int i = 0; i<layer3.size(); i++){
			System.out.print(" " + layer3.get(i));
		}
		System.out.println();
		System.out.print("Level 2:");
		for (int i = 0; i<layer2.size(); i++){
			System.out.print(" " + layer2.get(i));
		}
		System.out.println();
		System.out.print("Level 1:");
		for (int i = 0; i<layer1.size(); i++){
			System.out.print(" " + layer1.get(i));
		}
		System.out.println("\n");
		
		System.out.println(" After rearrangement...");
		System.out.print("Level 3:");
		for (int i = layer2.size()-1; i>= 0; i--){
			System.out.print(" " + layer2.get(i));
		}
		System.out.println();
		System.out.print("Level 2:");
		for (int i = layer1.size()-1; i>= 0; i--){
			System.out.print(" " + layer1.get(i));
		}
		System.out.println();
		System.out.print("Level 1:");
		for (int i = 0; i<layer3.size(); i++){
			System.out.print(" " + layer3.get(i));
		}
		System.out.println("\n");
	}
	public static void lionPosition(ArrayList<Lion> data, String place){
		int total = data.size();
		if (total == 0)
			return;
		int perCage = total / 3;
		ArrayList<String> layer1 = new ArrayList<String>();
		ArrayList<String> layer2 = new ArrayList<String>();
		ArrayList<String> layer3 = new ArrayList<String>();
		int cage2 = 2*perCage + ((total%3)/2);
		for (int i = 0; i < total; i++){
			if (i < perCage){
				layer1.add(data.get(i).toString());
			}
			else if (i < cage2){
				layer2.add(data.get(i).toString());
			}
			else{
				layer3.add(data.get(i).toString());
			}
		}
		
		System.out.println("Location: " + place);
		System.out.print("Level 3:");
		for (int i = 0; i<layer3.size(); i++){
			System.out.print(" " + layer3.get(i));
		}
		System.out.println();
		System.out.print("Level 2:");
		for (int i = 0; i<layer2.size(); i++){
			System.out.print(" " + layer2.get(i));
		}
		System.out.println();
		System.out.print("Level 1:");
		for (int i = 0; i<layer1.size(); i++){
			System.out.print(" " + layer1.get(i));
		}
		System.out.println("\n");
		
		System.out.println(" After rearrangement...");
		System.out.print("Level 3:");
		for (int i = layer2.size()-1; i>= 0; i--){
			System.out.print(" " + layer2.get(i));
		}
		System.out.println();
		System.out.print("Level 2:");
		for (int i = layer1.size()-1; i>= 0; i--){
			System.out.print(" " + layer1.get(i));
		}
		System.out.println();
		System.out.print("Level 1:");
		for (int i = 0; i<layer3.size(); i++){
			System.out.print(" " + layer3.get(i));
		}
		System.out.println("\n");
	}
	public static void eaglePosition(ArrayList<Eagle> data, String place){
		int total = data.size();
		if (total == 0)
			return;
		int perCage = total / 3;
		ArrayList<String> layer1 = new ArrayList<String>();
		ArrayList<String> layer2 = new ArrayList<String>();
		ArrayList<String> layer3 = new ArrayList<String>();
		int cage2 = 2*perCage + ((total%3)/2);
		for (int i = 0; i < total; i++){
			if (i < perCage){
				layer1.add(data.get(i).toString());
			}
			else if (i < cage2){
				layer2.add(data.get(i).toString());
			}
			else{
				layer3.add(data.get(i).toString());
			}
		}
		
		System.out.println("Location: " + place);
		System.out.print("Level 3:");
		for (int i = 0; i<layer3.size(); i++){
			System.out.print(" " + layer3.get(i));
		}
		System.out.println();
		System.out.print("Level 2:");
		for (int i = 0; i<layer2.size(); i++){
			System.out.print(" " + layer2.get(i));
		}
		System.out.println();
		System.out.print("Level 1:");
		for (int i = 0; i<layer1.size(); i++){
			System.out.print(" " + layer1.get(i));
		}
		System.out.println("\n");
		
		System.out.println(" After rearrangement...");
		System.out.print("Level 3:");
		for (int i = layer2.size()-1; i>= 0; i--){
			System.out.print(" " + layer2.get(i));
		}
		System.out.println();
		System.out.print("Level 2:");
		for (int i = layer1.size()-1; i>= 0; i--){
			System.out.print(" " + layer1.get(i));
		}
		System.out.println();
		System.out.print("Level 1:");
		for (int i = 0; i<layer3.size(); i++){
			System.out.print(" " + layer3.get(i));
		}
		System.out.println("\n");
	}
}