public class Cage{
	public static char indoor(int size){
		if (size < 45){
			return 'A';
		}
		if (size < 61){
			return 'B';
		}
		return 'C';
	}
	public static char outdoor(int size){
		if (size < 75){
			return 'A';
		}
		if (size < 91){
			return 'B';
		}
		return 'C';
	}
}