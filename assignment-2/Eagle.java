public class Eagle{
	private String name;
	private int length;
	private char cage;
	
	public Eagle(String name, int length, char cage){
		this.name = name;
		this.length = length;
		this.cage = cage;
	}
	public String getName(){
		return this.name;
	}
	public int getLength(){
		return this.length;
	}
	public char getCage(){
		return this.cage;
	}
	public String toString(){
		return this.getName() + " (" + this.getLength() + " - " + this.getCage() + ")";
	}
	public static String visited(String name){
		return "You are visiting " + name + " (eagle) now, what would you like to do? \n1: Order to fly\n";
	}
	public static void answer(String name, int n){
		if (n == 1){
			System.out.println(name + " makes a voice: kwaakk...");
			System.out.println("You hurt!");
		}
		else{
			System.out.println("You do nothing...");
		}
	}	
}