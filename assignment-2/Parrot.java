import java.util.Scanner;
public class Parrot{
	private String name;
	private int length;
	private char cage;
	
	public Parrot(String name, int length, char cage){
		this.name = name;
		this.length = length;
		this.cage = cage;
	}
	public String getName(){
		return this.name;
	}
	public int getLength(){
		return this.length;
	}
	public char getCage(){
		return this.cage;
	}
	public String toString(){
		return this.getName() + " (" + this.getLength() + " - " + this.getCage() + ")";
	}
	public static String visited(String name){
		return "You are visiting " + name + " (parrot) now, what would you like to do? \n1: Order to fly 2: Do conversation\n";
	}
	public static void answer(String name, int n){
		Scanner input = new Scanner(System.in);
		if (n == 1){
			System.out.println("Parrot " + name + " flies!");
			System.out.println(name + " makes a voice: FLYYYY….. ");
		}
		else if (n == 2){
			System.out.print("You say: "); String message = input.nextLine();
			if (message == ""){
				message = "HM?";
			}
			System.out.println(name + " says: " + message.toUpperCase());
		}
		else{
			System.out.println(name + " says: HM?");
		}
	}	
}