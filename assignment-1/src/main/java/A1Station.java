import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms
	static int lengthcar; // how much car requested
	static int currentcar = 0; // the current car
	static String name;
	static double weight;
	static double length;
	static WildCat cat;
	static TrainCar car;
	static TrainCar nextcar;

	public static void depart(TrainCar car) {
		System.out.println("The train departs to Javari Park");
		System.out.print("[LOCO]<--");
		car.printCar();
		double averageMassIndex = car.computeTotalMassIndex() / currentcar;
		System.out.printf("\nAverage mass index of all cats: %.2f\n", averageMassIndex);
		String category;
		if (averageMassIndex < 18.5) {
			category = "underweight";
		} else if (averageMassIndex < 25) {
			category = "normal";
		} else if (averageMassIndex < 30) {
			category = "overweight";
		} else {
			category = "obese";
		}
		System.out.printf("In average, the cats in the train are %s\n", category);
	}
	
    public static void main(String[] args) {
		Scanner read = new Scanner(System.in);
		lengthcar = Integer.parseInt(read.nextLine());
		String[] catcher;
        for (int i = 1; i <= lengthcar; i++) {
			currentcar += 1;
			catcher = read.nextLine().split(",");
			name = catcher[0];
			weight = Double.parseDouble(catcher[1]);
			length = Double.parseDouble(catcher[2]);
			cat = new WildCat(name, weight, length);
			if (currentcar == 1) {
				car = new TrainCar(cat);
			} else {
				car = new TrainCar(cat, nextcar);
			}
			nextcar = car;
			if (car.computeTotalWeight() >= THRESHOLD) {
				depart(car);
				currentcar = 0;
			}
		}
		if (car.computeTotalWeight() < THRESHOLD) {
			depart(car);
			}
    }
}
