public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms
	WildCat cat; // The cat within
	TrainCar next; // The next TrainCar

    public TrainCar(WildCat cat) {
        this.cat = cat;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
		this.next = next;
    }

    public double computeTotalWeight() {
        if (this.next == null) {
			return this.EMPTY_WEIGHT + this.cat.weight;
		}
        return this.EMPTY_WEIGHT + this.cat.weight + this.next.computeTotalWeight();
    }

    public double computeTotalMassIndex() {
        if (this.next == null) {
			return this.cat.computeMassIndex();
		}
        return this.cat.computeMassIndex() + this.next.computeTotalMassIndex();
    }

    public void printCar() {
        String string = "(" + this.cat.name + ")";
		System.out.print(string);
		if (this.next != null) {
			System.out.print("--");
			this.next.printCar();
		}
    }
}
